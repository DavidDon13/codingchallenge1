package src;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.*;

public class CompoundWordScanner {

    private Map wordMap = new LinkedHashMap<String, ArrayList<String>>();
    private ArrayList<String> compoundWordList = new ArrayList<String>();
    private String firstLongestCompoundWord = "";
    private String secondLongestCompoundWord = "";
    private TimingAuditor timer;
    private FileProcessor fileProcessor;
    ResultProcessor result;
    private boolean outputPerformanceAudit;
    private boolean outputWordCheckAudit;
    
    public static final int LETTER_GROUP_LENGTH = 5;
    private final int NO_OF_THREADS = 8;
    
    //These variables are used for quick-audit purposes to check that the program is operating corectly.
    private int audit_fileLineCount = 0;
    private int audit_wordsAssignedToMapCount = 0;
    private int audit_checked_wordCount = 0;
    private int audit_startingLetterGroupAssignedToMapCount = 0;
    private int audit_checked_letterGroupCount = 0;

    public CompoundWordScanner(String filename, boolean outputPerformanceAudit, boolean outputWordCheckAudit){
        this.timer = new TimingAuditor();
        this.result = new ResultProcessor();
        this.fileProcessor = new FileProcessor(filename, LETTER_GROUP_LENGTH);
        this.outputPerformanceAudit = outputPerformanceAudit;
        this.outputWordCheckAudit = outputWordCheckAudit;
    }
    
    public static void main(String[] args){
        CompoundWordScanner compoundWordScanner = new CompoundWordScanner("wordsforproblem.txt", false, false);
        compoundWordScanner.scanFileForCompoundWords();
    }
    
    private void scanFileForCompoundWords(){
        Result fileTaskResult = fileProcessor.processFile();
        if(fileTaskResult == Result.FILE_SUCCESS){
            timer.recordTime("Processing file");
            wordMap = fileProcessor.getWordMap();
            audit_wordsAssignedToMapCount = fileProcessor.getWordCount();
            audit_startingLetterGroupAssignedToMapCount = fileProcessor.getStartingLetterGroupCount();
            Result wordProcessingTaskResult = delegateWordProcessingWork();
            if(wordProcessingTaskResult == Result.WP_SUCCESS){
                timer.recordTime("Processing words");
                Result longestWordTaskResult = findFirstLongestCompoundWord();
                if (longestWordTaskResult == Result.LONGEST_SUCCESS){
                    timer.recordTime("Finding largest word");
                    result.setFirstLongestCompoundWord(firstLongestCompoundWord);
                    result.printResult(Result.LONGEST_SUCCESS);
                    Result slWordTaskResult = findSecondLongestCompoundWord();
                    if(slWordTaskResult == Result.SL_SUCCESS){
                        timer.recordTime("Finding second largest word");
                        result.setSecondLongestCompoundWord(secondLongestCompoundWord);
                        result.printResult(Result.SL_SUCCESS);
                    }
                    else{
                        result.printResult(slWordTaskResult);
                    }
                    result.setCompoundWordListCount(compoundWordList.size());
                    result.printResult(Result.COMPOUND_WORD_COUNT);
                    timer.recordEndingTime();
                    if(outputPerformanceAudit){
                        timer.printTimesAsPercentages();
                    }
                    if(outputWordCheckAudit){
                        printWordCheckAudit();
                    }
                }
                else{
                    result.printResult(longestWordTaskResult);
                }
            }
            else{
                result.printResult(wordProcessingTaskResult);
            }
        }
        else{
            result.printResult(fileTaskResult);
        }
    }

    /**
    This method delegates word processing work to threads to speed up the task.
    * Each thread takes a group of letters and compares words beginning with them to the entire set to find composite words.
    When the processing threads are finished, the main thread collects their individual lists and combines and sorts them.
    */  
    private Result delegateWordProcessingWork(){
        ExecutorService threadPool = Executors.newFixedThreadPool(NO_OF_THREADS);
        Set<Future<Map>> futureSet = new HashSet<Future<Map>>();
        Object[] keyArray = wordMap.keySet().toArray();
        
        for(Object key : keyArray){
            Callable<Map> callable = new WordProcessorThread((String) key, wordMap, LETTER_GROUP_LENGTH);
            Future<Map> future = threadPool.submit(callable);
            futureSet.add(future);
        }
        
        try{
            for(Future<Map> individualFeature: futureSet){
                compoundWordList.addAll( (ArrayList<String>) individualFeature.get().get("compositeWordList"));
                audit_checked_wordCount += (int) individualFeature.get().get("count");
                audit_checked_letterGroupCount += (int) individualFeature.get().get("letterGroupCheckedCount");
            }
        }
        catch(InterruptedException ie){
            return Result.WP_ERR_THREAD_INTERRUPTED;
        }
        catch(ExecutionException ee){
            return Result.WP_ERR_THREAD_EXECUTION_FAILED;
        }
        finally{
            threadPool.shutdown();
            Collections.sort(compoundWordList);
        }
        return Result.WP_SUCCESS;
    }
    
    /**
    This method gets the first of the longest words in the arrayList.
    If there are multiple words of the same length, the first found in the arrayList will be returned.
    */
    private Result findFirstLongestCompoundWord(){
        if (compoundWordList.isEmpty()){
            return Result.LONGEST_ERR_NO_COMPOUND_WORDS;
        }
        for (String compositeWord: compoundWordList){
            if (compositeWord.length() > firstLongestCompoundWord.length()){
                firstLongestCompoundWord = compositeWord;
            }
        }
        return Result.LONGEST_SUCCESS;
    }
    
    /**
    This method finds the first instance of the group of second longest words in the file. 
    */
    private Result findSecondLongestCompoundWord(){
        if (compoundWordList.size() < 2){
            return Result.SL_ERR_ONLY_ONE_WORD;
        }
        for (String compositeWord: compoundWordList){
            if ( (compositeWord.length() < firstLongestCompoundWord.length()) && (compositeWord.length() > secondLongestCompoundWord.length()) ){
                secondLongestCompoundWord = compositeWord;
            }
        }
        return Result.SL_SUCCESS;
    }
 
    /**
    This method sorts ArrayLists of Strings by word length - longest to shortest.
    It is used here for audit purposes to check for the longest words.
    */
    public static void sortArrayListByStringLength(ArrayList<String> compositeWordList){
        Collections.sort(compositeWordList, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                if(s1.length() < s2.length()) {
                    return 1;
                } else if(s1.length() == s2.length()) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
    }
    
    private void printWordCheckAudit(){
        System.out.println();
        System.out.println("Valid words in file count: " + audit_wordsAssignedToMapCount + "; Letter groups in file count: " + audit_startingLetterGroupAssignedToMapCount);
        System.out.println("Words checked for compounds: " + audit_checked_wordCount + "; Letter groups Checked for compounds: " + audit_checked_letterGroupCount);
    }
}