package src;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TimingAuditor {
    private long startingTime;
    private Map timeMap;
    private long endingTime;
    private long totalExecutionTime;
    private long lastStoredTime;
    
    public TimingAuditor(){
        startingTime = convertNanoToMilli(System.nanoTime());
        timeMap = new LinkedHashMap<String, Long>();
    }
    
    private long convertNanoToMilli(Long nanoTime){
        return nanoTime / 1000000;
    }
    
    public void recordTime(String label){
        long currentTime = convertNanoToMilli(System.nanoTime());
        long newTime;
        if(timeMap.isEmpty()){
            newTime = currentTime - startingTime;
        }
        else{
            newTime = currentTime - lastStoredTime;
        }
        timeMap.put(label, newTime);
        lastStoredTime = currentTime;
    }
    
    public void recordEndingTime(){
        endingTime = convertNanoToMilli(System.nanoTime());
        calculateTotalExecutionTime();
    }
    
    private void calculateTotalExecutionTime(){
        totalExecutionTime = endingTime - startingTime;
    }
    
    public long getTotalExecutionTime(){
        return totalExecutionTime;
    }
    
    public void printTimes(){
        System.out.println();
        System.out.println("Total execution time: " + getTotalExecutionTime() + " ms");
        for(Entry<String, Long> entry: (Set<Entry>) timeMap.entrySet()){
            System.out.println(entry.getKey() + ": " + entry.getValue() + " ms");
        }
    }
    
    public void printTimesAsPercentages(){
        System.out.println();
        System.out.println("Total execution time: " + getTotalExecutionTime() + " ms");
        for(Entry<String, Long> entry: (Set<Entry>) timeMap.entrySet()){
            System.out.println( entry.getKey() + ": " + ( ((double) entry.getValue()) / totalExecutionTime * 100) + " %");
        }
    }
}