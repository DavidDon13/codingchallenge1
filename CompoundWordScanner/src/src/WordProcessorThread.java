package src;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class WordProcessorThread implements Callable {
//    private ArrayList<String> letterArrayList;
    private String letterGroup;
    private Map wordMap;
    private final int LETTER_GROUP_LENGTH;
    private ArrayList<String> compositeWordList;
    private int wordsChecked = 0;
    private int letterGroupCheckedCount = 0;

    public WordProcessorThread( String letterGroup, Map wordMap, int letterGroupLength ){
        this.letterGroup = letterGroup;
        this.wordMap = wordMap;
        this.LETTER_GROUP_LENGTH = letterGroupLength;
        this.compositeWordList = new ArrayList<String>();
    }
    
    @Override
    public Map call() {
        processGroupOfWords();
        Map map = new HashMap<String, ArrayList<String>>();
        map.put("count", wordsChecked);
        map.put("compositeWordList", compositeWordList);
        map.put("letterGroupCheckedCount", letterGroupCheckedCount);
        return map;
    }
    
    /**
    This method takes each word from the list and passes it to the processWord method. 
    Composite words return true values and are added to a special list.
     * @return 
    */
    public void processGroupOfWords(){
        letterGroupCheckedCount++;
        ArrayList<String> particularList = (ArrayList<String>) wordMap.get(letterGroup);
        for(String singleWord: particularList){
            wordsChecked++;
            boolean isCompositeWord = processWord(singleWord, true);
            if (isCompositeWord == true){
                compositeWordList.add(singleWord);
            }
        }
    }
    
    /**
    This method takes a word and checks all words that begin with the same letter in the set against it.
    If the primary word begins with the word from the set, they are subtracted and the remainder is processed recursively.
    This process continues until the remainder is either found or not found in the set.
    If the remainder is found then a value of true is returned and the primary word is a composite.
    If the remainder is not found, then a value of false is returned and the primary word is not a composite.
    */
    private boolean processWord(String word, boolean firstTime){
        boolean returnValue = false;
        String startingLetterGroup = getStartingLetterGroupOfWord(word, LETTER_GROUP_LENGTH);
        ArrayList<String> shortList = null;
        
        outerLoop:
        for(int i = 0; i < startingLetterGroup.length(); i++){
            shortList = (ArrayList<String>) wordMap.get(startingLetterGroup.substring(0, i+1));
            if (shortList != null){
                for(String singleWord: shortList){
                    if (word.startsWith(singleWord) ){
                        if ( word.equals(singleWord) && (firstTime == true) ) {
                            returnValue = false;
                        }
                        else if ( word.equals(singleWord) && (firstTime == false) ) {
                            returnValue = true;
                            break;
                        }
                        else{
                            returnValue = processWord(word.substring(singleWord.length()), false);
                            if (returnValue == true){
                                break outerLoop;
                            }
                        }
                    }
                    else{
                        returnValue = false;
                    }
                }
            }
        }
        return returnValue;
    }
    
    /**
    This method returns the group of letters at the beginning of a word.
    If the word is longer than the predefined group length, the group is returned using substring.
    If the word is only as long or shorter than the predefined length, then it is returned as is.
    */
    private String getStartingLetterGroupOfWord(String word, int groupLength){
        if (word.length() > groupLength){
            return word.substring(0,groupLength);
        }
        return word;
    }
}