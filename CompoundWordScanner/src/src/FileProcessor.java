package src;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class FileProcessor {
    private final String fileName;
    private ArrayList<String> rawWordArrayList;
    private Map wordMap;
    private int fileLineCount = 0;
    private int wordCount = 0;
    private int startingLetterGroupCount = 0;
    public final int LETTER_GROUP_LENGTH;
    
    public FileProcessor(String fileName, int letterGroupLength){
        this.fileName = fileName;
        this.rawWordArrayList = new ArrayList<String>();
        this.wordMap = new LinkedHashMap<String, ArrayList<String>>();
        this.LETTER_GROUP_LENGTH = letterGroupLength;
    }
    
    public Result processFile(){
        Result fileTaskResultCode = readFileIntoArrayList();
        if(fileTaskResultCode == Result.FILE_SUCCESS){
            subDivideRawListIntoMapOfLists();
            return Result.FILE_SUCCESS;
        }
        else{
            return fileTaskResultCode;
        }
    }
    
    /**
    This method reads a file into an arrayList. The while loop filters out null and blank values from the file.
    */
    private Result readFileIntoArrayList(){
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            String singleLine;
            while ( ((singleLine = bufferedReader.readLine()) != null) && (!singleLine.equals(""))){
                fileLineCount++;
                rawWordArrayList.add(singleLine);
            } 
        }
        catch(IOException io){
            return Result.FILE_ERR_NOT_FOUND;
        }
        if (rawWordArrayList.isEmpty()){
            return Result.FILE_ERR_CONTAINS_NO_VALID_CONTENT;
        }
        return Result.FILE_SUCCESS;
    }
    
    private void subDivideRawListIntoMapOfLists() {
        String startingLetterGroup = null;
        String newStartingLetterGroup = null;
        ArrayList<String> arrayListPerLetter = new ArrayList<String>();
        Map mapPerLetter = new LinkedHashMap<String, ArrayList<String>>();
        String startingLetter = null;
        String newStartingLetter = null;
        
        //This block deals with the first word in the arrayList...
        startingLetter = getStartingLetterGroupOfWord(rawWordArrayList.get(0), 1);
        
        startingLetterGroup = getStartingLetterGroupOfWord(rawWordArrayList.get(0), LETTER_GROUP_LENGTH);
        arrayListPerLetter.add(rawWordArrayList.get(0));
        wordCount++;
        
        //...the next block deals with the 2nd to 2nd last words and part of the first stage of processing the final word...
        for (String word: rawWordArrayList){
            newStartingLetter = getStartingLetterGroupOfWord(word, 1);
            newStartingLetterGroup = getStartingLetterGroupOfWord(word, LETTER_GROUP_LENGTH);
            if (startingLetterGroup.equals(newStartingLetterGroup)) {
                arrayListPerLetter.add(word);
                wordCount++;
            }
            else{
                wordMap.put(startingLetterGroup, arrayListPerLetter);
                startingLetterGroupCount++;
                arrayListPerLetter = new ArrayList<String>();
                startingLetterGroup = newStartingLetterGroup;
                arrayListPerLetter.add(word);
                wordCount++;
            }
        }

        //...the next block deals with the second stage of processing the final word...
        wordMap.put(startingLetterGroup, arrayListPerLetter);
        startingLetterGroupCount++;
    }
    
    /**
    This method returns the group of letters at the beginning of a word.
    If the word is longer than the predefined group length, the group is returned using substring.
    If the word is only as long or shorter than the predefined length, then it is returned as is.
    */
    private String getStartingLetterGroupOfWord(String word, int groupLength){
        if (word.length() > groupLength){
            return word.substring(0,groupLength);
        }
        return word;
    }
    
    public Map getWordMap(){
        return wordMap;
    }
    
    public int getFileLineCount(){
        return fileLineCount;
    }
    
    public int getWordCount(){
        return wordCount;
    }
    
    public int getStartingLetterGroupCount(){
        return startingLetterGroupCount;
    }
}