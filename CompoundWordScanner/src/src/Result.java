package src;

public enum Result{
    FILE_ERR_NOT_FOUND,
    FILE_ERR_CONTAINS_NO_VALID_CONTENT,
    FILE_SUCCESS,
    WP_ERR_THREAD_INTERRUPTED,
    WP_ERR_THREAD_EXECUTION_FAILED,
    WP_SUCCESS,
    LONGEST_ERR_NO_COMPOUND_WORDS,
    LONGEST_SUCCESS,
    SL_ERR_ONLY_ONE_WORD,
    SL_SUCCESS,
    COMPOUND_WORD_COUNT;
    
    @Override
    public String toString(){
        String resultString = "";
        if(this.ordinal() == 0){
            resultString = "The file was not found.";
        }
        else if(this.ordinal() == 1){
            resultString = "The file does not contain any words.";
        }
        else if(this.ordinal() == 2){
            resultString = "The file was successfully processed.";
        }
        else if(this.ordinal() == 3){
            resultString = "The word processing threads were interrupted.";
        }
        else if(this.ordinal() == 4){
            resultString = "The word processing threads failed to execute.";
        }
        else if(this.ordinal() == 5){
            resultString = "There words were successfully processed.";
        }
        else if(this.ordinal() == 6){
            resultString = "There are no compound words in the file.";
        }
        else if(this.ordinal() == 7){
            resultString = "The first longest word is: ";
        }
        else if(this.ordinal() == 8){
            resultString = "There is no second longest word because there is only 1 compound word.";
        }
        else if(this.ordinal() == 9){
            resultString = "The second longest word is: ";
        }
        else if(this.ordinal() == 10){
            resultString = "The total number of compound words is: ";
        }
        return resultString;
    }
}
