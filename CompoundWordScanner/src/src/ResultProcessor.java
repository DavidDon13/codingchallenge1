package src;

public class ResultProcessor {
    
    private String firstLongestCompoundWord;
    private String secondLongestCompoundWord;
    private int compoundWordListCount;
            
    public ResultProcessor(){
        
    }
    
    public void setFirstLongestCompoundWord(String word){
        this.firstLongestCompoundWord = word;
    }
    
    public void setSecondLongestCompoundWord(String word){
        this.secondLongestCompoundWord = word;
    }
    
    public void setCompoundWordListCount(int count){
        this.compoundWordListCount = count;
    }
    
    public String getResultFeedback(Result result){
        String feedback = "";
        if (result == Result.LONGEST_SUCCESS){
            feedback = result.toString() + firstLongestCompoundWord + " (" + firstLongestCompoundWord.length() + " characters)";
        }
        else if (result == Result.SL_SUCCESS){
            feedback = result.toString() + secondLongestCompoundWord + " (" + secondLongestCompoundWord.length() + " characters)";
        }
        else if (result == Result.COMPOUND_WORD_COUNT){
            feedback = result.toString() + compoundWordListCount;
        }
        else{
            feedback = result.toString();
        }
        return feedback;
    }
    
    public void printResult(Result result){
        System.out.println( getResultFeedback(result) );
    }
}
